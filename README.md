# Technical Test

This project is an implementation for the following technical test I was asked to realize.

## The test
### Data

The app must present a list of albums that can be retrieved from the following URL: [https://img8.leboncoin.fr/technical-test.json](https://img8.leboncoin.fr/technical-test.json)

### Prerequisites

- The project must be realized on the Android platform (API 15+)
- You must implement a data persistence system so that data can be available without any network connection. Data must be available after a restart.
- You are free to use the language and libraries you want, but you must explain your choices.
- Your code must be versioned on a freely searchable Git repository

### What will be watched

- Architecture choices
- Patterns choices
- Configuration changes management
- Application performances

## Choices I made

### Language

**Kotlin** provides features enabling more concise and understandable code. This allows developers to write less code without any sacrifice on comprehension. Being 100% Java interoperable, it can be integrated smoothly into any existing Java project.

Now that Google is officially supporting Kotlin and suggests it as the default option in Android Studio, I do not think it is really relevant anymore to provide more arguments.

### Architecture

I knew about the MVI architecture when Hannes Dorfmann talked about it in 2016 in his  [Model-View-Intent on Android](http://hannesdorfmann.com/android/model-view-intent) blog post and experimented myself [Boilerplate-Android](https://bitbucket.org/Gorcyn/boilerplate-android/src/master/).

Since, Jake Wharton talked about the idea of [Managing State with RxJava](https://www.youtube.com/watch?v=0IKHxjkgop4) during Devoxx.

Last year, a MVI Java implementation has been added to the official android architecture google samples, as an [external one](https://github.com/googlesamples/android-architecture#external-samples). Its Kotlin implementation is also [available](https://github.com/oldergod/android-architecture/tree/todo-mvi-rxjava-kotlin).

### Libraries

#### Network communication

Retrofit is a type-safe REST adapter that makes easy common networking tasks. 
Retrofit let you define your REST API as a simple Java interface and uses annotations 
to describe the HTTP request (headers, parameters, etc.). This library then takes care of URL manipulation, requesting, loading, caching, threading, synchronization.

Retrofit is the [most used network library](https://www.appbrain.com/stats/libraries/tag/network/android-network-libraries).

#### Data management

Room, the official Google data persistence library introduced with the Android architecture components.

Room provides an abstraction layer over SQLite letting us define data objects as simple POJOs and data access object as simple interfaces. Room also validate SQL queries are compile time. 

#### Image loading

Glide is an efficiant image loading library. It wraps media decoding, disk and memory caching.

Glide was presented as a good choice by Google during one of its Google Developer Summit in Thailand some years ago and is still used in the [Google I/O app](https://github.com/google/iosched/blob/master/build.gradle#L51).

Glide is the [most used image loading library](https://www.appbrain.com/stats/libraries/tag/image-loader/android-image-loaders).

#### MVI

RxJava and ViewModel (from the Google architecture components) are part of the MVI architecture.