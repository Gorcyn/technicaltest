package com.gorcyn.technicaltest.data.source

import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when`
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

import org.hamcrest.CoreMatchers.`is`

import io.reactivex.observers.TestObserver
import io.reactivex.Maybe
import io.reactivex.Single

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.local.AlbumLocalDataSource
import com.gorcyn.technicaltest.data.source.remote.AlbumRemoteDataSource
import com.gorcyn.technicaltest.tests.RxTest

class AlbumRepositoryTest: RxTest() {

    companion object {
        private val ALBUM_LIST = listOf(
                Album(1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952", 1),
                Album(2, "reprehenderit est deserunt velit ipsam", "https://via.placeholder.com/600/771796",  "https://via.placeholder.com/150/771796", 1),
                Album(3, "officia porro iure quia iusto qui ipsa ut modi", "https://via.placeholder.com/600/24f355", "https://via.placeholder.com/150/24f355", 1)
        )
    }

    @Mock
    private lateinit var remoteDataSource: AlbumRemoteDataSource
    @Mock
    private lateinit var localDataSource: AlbumLocalDataSource

    private lateinit var albumRepository: AlbumRepository
    private lateinit var testObserver: TestObserver<List<Album>>

    @Before
    fun setupAlbumRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        // Get a reference to the class under test
        albumRepository = AlbumRepository.getInstance(localDataSource, remoteDataSource)

        testObserver = TestObserver()
    }

    @After
    fun destroyRepositoryInstance() {
        AlbumRepository.clearInstance()
    }

    @Test
    fun getAlbumList_repositoryCachesAfterFirstSubscription_whenAlbumListAvailableInLocalStorage() {
        // Given that the local data source has data available
        setAlbumListAvailable(localDataSource, ALBUM_LIST)
        // And the remote data source does not have any data available
        setAlbumListNotAvailable(remoteDataSource)

        // When two subscriptions are set
        val testObserver1 = TestObserver<List<Album>>()
        albumRepository.fetchAlbumList().subscribe(testObserver1)

        val testObserver2 = TestObserver<List<Album>>()
        albumRepository.fetchAlbumList().subscribe(testObserver2)

        // Then albums were only requested once from remote and local sources
        verify<AlbumDataSource>(remoteDataSource).fetchAlbumList()
        verify<AlbumDataSource>(localDataSource).fetchAlbumList()
        //
        assertFalse(albumRepository.cacheIsDirty)
        testObserver1.assertValue(ALBUM_LIST)
        testObserver2.assertValue(ALBUM_LIST)
    }

    @Test
    fun getAlbumList_repositoryCachesAfterFirstSubscription_whenAlbumListAvailableInRemoteStorage() {
        // Given that the local data source has data available
        setAlbumListAvailable(remoteDataSource, ALBUM_LIST)
        // And the remote data source does not have any data available
        setAlbumListNotAvailable(localDataSource)

        // When two subscriptions are set
        val testObserver1 = TestObserver<List<Album>>()
        albumRepository.fetchAlbumList().subscribe(testObserver1)

        val testObserver2 = TestObserver<List<Album>>()
        albumRepository.fetchAlbumList().subscribe(testObserver2)

        // Then albums were only requested once from remote and local sources
        verify<AlbumDataSource>(remoteDataSource).fetchAlbumList()
        verify<AlbumDataSource>(localDataSource).fetchAlbumList()
        //
        assertFalse(albumRepository.cacheIsDirty)
        testObserver1.assertValue(ALBUM_LIST)
        testObserver2.assertValue(ALBUM_LIST)
    }

    @Test
    fun getAlbumList_requestsAlbumListFromLocalDataSource() {
        // Given that the local data source has data available
        setAlbumListAvailable(localDataSource, ALBUM_LIST)
        // And the remote data source does not have any data available
        setAlbumListNotAvailable(remoteDataSource)

        // When albums are requested from the album repository
        albumRepository.fetchAlbumList().subscribe(testObserver)

        // Then albums are loaded from the local data source
        verify<AlbumDataSource>(localDataSource).fetchAlbumList()
        testObserver.assertValue(ALBUM_LIST)
    }

    @Test
    fun saveAlbum_savesAlbumToServiceAPI() {
        // Given a stub album
        val newAlbum = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)

        // When an album is saved to the album repository
        albumRepository.saveAlbum(newAlbum)

        // Then the service API and persistent repository are called and the cache is updated
        verify<AlbumDataSource>(remoteDataSource).saveAlbum(newAlbum)
        verify<AlbumDataSource>(localDataSource).saveAlbum(newAlbum)
        assertThat(albumRepository.cachedAlbumList!!.size, `is`(1))
    }

    @Test
    fun getAlbum_requestsSingleAlbumFromLocalDataSource() {
        // Given a stub completed album in the local repository
        val album = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)
        setAlbumAvailable(localDataSource, album)
        // And the album not available in the remote repository
        setAlbumNotAvailable(remoteDataSource, album.id)

        // When an album is requested from the album repository
        val testObserver = TestObserver<Album>()
        albumRepository.fetchAlbum(album.id).subscribe(testObserver)

        // Then the album is loaded from the database
        verify<AlbumDataSource>(localDataSource).fetchAlbum(eq(album.id))
        testObserver.assertValue(album)
    }

    @Test
    fun getAlbumListWithDirtyCache_albumListIsRetrievedFromRemote() {
        // Given that the remote data source has data available
        setAlbumListAvailable(remoteDataSource, ALBUM_LIST)

        // When calling fetchAlbumList in the repository with dirty cache
        albumRepository.refreshAlbumList()
        albumRepository.fetchAlbumList().subscribe(testObserver)

        // Verify the albums from the remote data source are returned, not the local
        verify<AlbumDataSource>(localDataSource, never()).fetchAlbumList()
        verify<AlbumDataSource>(remoteDataSource).fetchAlbumList()
        testObserver.assertValue(ALBUM_LIST)
    }

    @Test
    fun getAlbumListWithLocalDataSourceUnavailable_albumListIsRetrievedFromRemote() {
        // Given that the local data source has no data available
        setAlbumListNotAvailable(localDataSource)
        // And the remote data source has data available
        setAlbumListAvailable(remoteDataSource, ALBUM_LIST)

        // When calling fetchAlbumList in the repository
        albumRepository.fetchAlbumList().subscribe(testObserver)

        // Verify the albums from the remote data source are returned
        verify<AlbumDataSource>(remoteDataSource).fetchAlbumList()
        testObserver.assertValue(ALBUM_LIST)
    }

    @Test
    fun getAlbumListWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given that the local data source has no data available
        setAlbumListNotAvailable(localDataSource)
        // And the remote data source has no data available
        setAlbumListNotAvailable(remoteDataSource)

        // When calling fetchAlbumList in the repository
        albumRepository.fetchAlbumList().subscribe(testObserver)

        // Verify no data is returned
        testObserver.assertNoValues()
        // Verify that error is returned
        testObserver.assertError(NoSuchElementException::class.java)
    }

    @Test
    fun getAlbumWithBothDataSourcesUnavailable_firesOnError() {
        // Given an album id
        val albumId = 123L
        // And the local data source has no data available
        setAlbumNotAvailable(localDataSource, albumId)
        // And the remote data source has no data available
        setAlbumNotAvailable(remoteDataSource, albumId)

        // When calling fetchAlbum in the repository
        val testObserver = TestObserver<Album>()
        albumRepository.fetchAlbum(albumId).subscribe(testObserver)

        // Verify that error is returned
        testObserver.assertError(NoSuchElementException::class.java)
    }

    @Test
    fun getAlbumList_refreshesLocalDataSource() {
        // Given that the remote data source has data available
        setAlbumListAvailable(remoteDataSource, ALBUM_LIST)

        // Mark cache as dirty to force a reload of data from remote data source.
        albumRepository.refreshAlbumList()

        // When calling fetchAlbumList in the repository
        albumRepository.fetchAlbumList().subscribe(testObserver)

        // Verify that the data fetched from the remote data source was saved in local.
        verify<AlbumDataSource>(localDataSource, times(ALBUM_LIST.size)).saveAlbum(any())
        testObserver.assertValue(ALBUM_LIST)
    }

    private fun setAlbumListNotAvailable(dataSource: AlbumDataSource) {
        `when`(dataSource.fetchAlbumList()).thenReturn(Single.just(emptyList()))
    }

    private fun setAlbumListAvailable(dataSource: AlbumDataSource, albumList: List<Album>) {
        // don't allow the data sources to complete.
        `when`(dataSource.fetchAlbumList()).thenReturn(
                Single.just(albumList).concatWith(Single.never()).firstOrError())
    }

    private fun setAlbumNotAvailable(dataSource: AlbumDataSource, albumId: Long) {
        `when`(dataSource.fetchAlbum(eq(albumId)))
                .thenReturn(Maybe.error(NoSuchElementException("The MaybeSource is empty")))
    }

    private fun setAlbumAvailable(dataSource: AlbumDataSource, album: Album) {
        `when`(dataSource.fetchAlbum(eq(album.id))).thenReturn(Maybe.just(album))
    }
}
