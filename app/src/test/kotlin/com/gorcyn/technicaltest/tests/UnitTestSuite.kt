package com.gorcyn.technicaltest.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.technicaltest.data.source.AlbumRepositoryTest
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailViewModelTest
import com.gorcyn.technicaltest.ui.albumList.AlbumListViewModelTest

@RunWith(Suite::class)

@Suite.SuiteClasses(
        AlbumListViewModelTest::class,
        AlbumDetailViewModelTest::class,
        AlbumRepositoryTest::class
)
class UnitTestSuite
