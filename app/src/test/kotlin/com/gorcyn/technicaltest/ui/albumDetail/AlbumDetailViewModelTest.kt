package com.gorcyn.technicaltest.ui.albumDetail

import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

import com.nhaarman.mockito_kotlin.eq

import io.reactivex.observers.TestObserver
import io.reactivex.Maybe
import io.reactivex.Observable

import com.gorcyn.technicaltest.Injection
import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.tests.RxTest
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailIntent.InitialIntent

class AlbumDetailViewModelTest: RxTest() {

    @Mock
    private lateinit var albumDataSource: AlbumDataSource

    private lateinit var albumDetailViewModel: AlbumDetailViewModel
    private lateinit var testObserver: TestObserver<AlbumDetailViewState>

    @Before
    fun setupAlbumDetailViewModel() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        albumDetailViewModel = AlbumDetailViewModel(
                AlbumDetailActionProcessorHolder(albumDataSource, Injection.provideSchedulerProvider())
        )

        testObserver = albumDetailViewModel.states().test()
    }

    @Test
    fun populateAlbum_callsRepoAndUpdatesViewOnSuccess() {
        val testAlbum = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)
        `when`(albumDataSource.fetchAlbum(testAlbum.id)).thenReturn(Maybe.just(testAlbum))

        // When populating an album is initiated by an initial intent
        albumDetailViewModel.processIntents(Observable.just(InitialIntent(testAlbum.id)))

        // Then the album repository is queried and a state is emitted back
        verify(albumDataSource).fetchAlbum(eq(testAlbum.id))
        testObserver.assertValueAt(2) { (_, album) ->
            album?.title == testAlbum.title && album.url == testAlbum.url
        }
    }

    @Test
    fun populateAlbum_callsRepoAndUpdatesViewOnError() {
        val testAlbum = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)
        `when`(albumDataSource.fetchAlbum(testAlbum.id))
                .thenReturn(Maybe.error(NoSuchElementException("The MaybeSource is empty")))

        // When populating an album is initiated by an initial intent
        albumDetailViewModel.processIntents(Observable.just(InitialIntent(testAlbum.id)))

        // Then the album repository is queried and a state is emitted back
        verify(albumDataSource).fetchAlbum(eq(testAlbum.id))
        testObserver.assertValueAt(2) { (_, _, error) ->
            error != null
        }
    }
}
