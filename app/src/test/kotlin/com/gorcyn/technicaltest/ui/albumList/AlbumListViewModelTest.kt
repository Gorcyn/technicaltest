package com.gorcyn.technicaltest.ui.albumList

import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

import com.nhaarman.mockito_kotlin.any

import io.reactivex.observers.TestObserver
import io.reactivex.Observable
import io.reactivex.Single

import com.gorcyn.technicaltest.Injection
import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.tests.RxTest

class AlbumListViewModelTest: RxTest() {

    @Mock
    private lateinit var albumDataSource: AlbumDataSource

    private lateinit var albumListViewModel: AlbumListViewModel
    private lateinit var testObserver: TestObserver<AlbumListViewState>
    private lateinit var albumList: List<Album>

    @Before
    fun setupAlbumListViewModel() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        albumListViewModel = AlbumListViewModel(
                AlbumListActionProcessorHolder(albumDataSource, Injection.provideSchedulerProvider())
        )

        albumList = listOf(
                Album(1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952", 1),
                Album(2, "reprehenderit est deserunt velit ipsam", "https://via.placeholder.com/600/771796",  "https://via.placeholder.com/150/771796", 1),
                Album(3, "officia porro iure quia iusto qui ipsa ut modi", "https://via.placeholder.com/600/24f355", "https://via.placeholder.com/150/24f355", 1)
        )

        testObserver = albumListViewModel.states().test()
    }

    @Test
    fun loadAllAlbumsFromRepositoryAndLoadIntoView() {
        // Given an initialized AlbumListViewModel with initialized albums
        `when`(albumDataSource.fetchAlbumList(any())).thenReturn(Single.just(albumList))

        // When loading of Albums is initiated
        albumListViewModel.processIntents(Observable.just(AlbumListIntent.InitialIntent))

        // Then progress indicator state is emitted
        testObserver.assertValueAt(1, AlbumListViewState::isLoading)
        // Then progress indicator state is canceled and all albums are emitted
        testObserver.assertValueAt(2) { albumListViewState -> !albumListViewState.isLoading }
    }

    @Test
    fun errorLoadingAlbumList_ShowsError() {
        // Given that no albums are available in the repository
        `when`(albumDataSource.fetchAlbumList(any())).thenReturn(Single.error(Exception()))

        // When albums are loaded
        albumListViewModel.processIntents(Observable.just(AlbumListIntent.InitialIntent))

        // Then an error containing state is emitted
        testObserver.assertValueAt(2) { state -> state.error != null }
    }
}
