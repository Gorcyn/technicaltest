package com.gorcyn.technicaltest.data.source

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album

interface AlbumDataSource {

    fun fetchAlbumList(forceUpdate: Boolean): Single<List<Album>> {
        if (forceUpdate) refreshAlbumList()
        return fetchAlbumList()
    }

    fun fetchAlbumList(): Single<List<Album>>
    fun fetchAlbum(albumId: Long): Maybe<Album>
    fun saveAlbum(album: Album): Completable
    fun refreshAlbumList()
    fun deleteAlbumList(): Completable
}
