package com.gorcyn.technicaltest.data.source

import android.support.annotation.VisibleForTesting

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.util.SingletonHolderDoubleArg

open class AlbumRepository(
        private val localDataSource: AlbumDataSource,
        private val remoteDataSource: AlbumDataSource
): AlbumDataSource {

    companion object : SingletonHolderDoubleArg<AlbumRepository, AlbumDataSource, AlbumDataSource>(::AlbumRepository)

    @VisibleForTesting
    var cachedAlbumList: MutableMap<Long, Album>? = null
    @VisibleForTesting
    var cacheIsDirty = false

    override fun refreshAlbumList() {
        cacheIsDirty = true
    }

    /**
     * Gets albums from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     */
    override fun fetchAlbumList(): Single<List<Album>> {
        // Respond immediately with cache if available and not dirty
        if (cachedAlbumList != null && !cacheIsDirty) {
            return Single.just(cachedAlbumList!!.values.toList())
        } else if (cachedAlbumList == null) {
            cachedAlbumList = LinkedHashMap()
        }

        val remoteAlbumList = getAndSaveRemoteAlbumList()
        return if (cacheIsDirty) {
            remoteAlbumList
                    .onErrorResumeNext { getAndCacheLocalAlbumList() }
        } else {
            val localAlbumList = getAndCacheLocalAlbumList()
            // Query the local storage if available. If not, query the network.
            Single.concat(localAlbumList, remoteAlbumList)
                    .filter { !it.isEmpty() }
                    .firstOrError()
        }
    }

    override fun fetchAlbum(albumId: Long): Maybe<Album> {
        if (cachedAlbumList != null && !cacheIsDirty) {
            // Respond immediately with cache if available
            val album = getAlbumWithId(albumId)
            if (album != null) {
                return Maybe.just(album)
            }
        } else if (cachedAlbumList == null) {
            cachedAlbumList = LinkedHashMap()
        }

        // Is the album in the local data source? If not, query the network.
        return remoteDataSource.fetchAlbum(albumId)
                .onErrorResumeNext(getAlbumWithIdFromLocalRepository(albumId))
    }

    override fun saveAlbum(album: Album): Completable {
        remoteDataSource.saveAlbum(album)
        localDataSource.saveAlbum(album)

        // Do in memory cache update to keep the app UI up to date
        if (cachedAlbumList == null) {
            cachedAlbumList = LinkedHashMap()
        }
        cachedAlbumList!![album.id] = album
        return Completable.complete()
    }

    override fun deleteAlbumList(): Completable {
        remoteDataSource.deleteAlbumList()
        localDataSource.deleteAlbumList()

        // Do in memory cache update to keep the app UI up to date
        cachedAlbumList = LinkedHashMap()
        return Completable.complete()
    }

    private fun getAndCacheLocalAlbumList(): Single<List<Album>> {
        return localDataSource.fetchAlbumList()
                .flatMap {
                    Observable.fromIterable(it)
                            .doOnNext { album -> cachedAlbumList!![album.id] = album }
                            .toList()
                }
    }

    private fun getAndSaveRemoteAlbumList(): Single<List<Album>> {
        return remoteDataSource.fetchAlbumList()
                .flatMap {
                    Observable.fromIterable(it)
                            .doOnNext { album ->
                                localDataSource.saveAlbum(album)
                                cachedAlbumList!![album.id] = album
                            }
                            .toList()
                }
                .doOnSuccess { cacheIsDirty = false }
    }

    private fun getAlbumWithId(id: Long): Album? = cachedAlbumList?.get(id)

    private fun getAlbumWithIdFromLocalRepository(albumId: Long): Maybe<Album> {
        return localDataSource.fetchAlbum(albumId)
                .doOnSuccess { cachedAlbumList!![albumId] = it }
    }
}
