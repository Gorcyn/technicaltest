package com.gorcyn.technicaltest.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.OnConflictStrategy

import io.reactivex.Maybe
import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album

@Dao
interface AlbumDao {

    @Query("SELECT * FROM album")
    fun getAll(): Single<List<Album>>

    @Query("SELECT * FROM album WHERE id = :id")
    fun findById(id: Long): Maybe<Album>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(album: Album)

    @Query("DELETE FROM album")
    fun deleteAll()
}
