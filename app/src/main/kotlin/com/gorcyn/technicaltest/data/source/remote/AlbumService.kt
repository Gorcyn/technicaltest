package com.gorcyn.technicaltest.data.source.remote

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album

interface AlbumService {

    @GET("technical-test.json")
    fun fetchAlbumList(): Single<List<Album>>

    companion object {
        val instance: AlbumService by lazy {
            Retrofit.Builder()
                    .baseUrl("https://img8.leboncoin.fr/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(AlbumService::class.java)
        }
    }
}
