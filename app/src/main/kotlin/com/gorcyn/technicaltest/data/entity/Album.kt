package com.gorcyn.technicaltest.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable

import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "album")
data class Album(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Long,

        @ColumnInfo(name = "title")
        var title: String,

        @ColumnInfo(name = "url")
        var url: String,

        @ColumnInfo(name = "thumbnailUrl")
        var thumbnailUrl: String,

        @ColumnInfo(name = "albumId")
        var albumId: Long
): Parcelable
