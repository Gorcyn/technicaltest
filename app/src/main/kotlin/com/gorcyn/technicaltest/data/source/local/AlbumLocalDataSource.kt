package com.gorcyn.technicaltest.data.source.local

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

import com.gorcyn.technicaltest.data.AppDatabase
import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.util.SingletonHolderSingleArg

open class AlbumLocalDataSource(
        private val appDatabase: AppDatabase
): AlbumDataSource {

    companion object : SingletonHolderSingleArg<AlbumLocalDataSource, AppDatabase>(::AlbumLocalDataSource)

    override fun fetchAlbumList(): Single<List<Album>> {
        return appDatabase.albumDao().getAll()
    }

    override fun fetchAlbum(albumId: Long): Maybe<Album> {
        return appDatabase.albumDao().findById(albumId)
    }

    override fun saveAlbum(album: Album): Completable {
        appDatabase.albumDao().save(album)
        return Completable.complete()
    }

    override fun refreshAlbumList() {
        // Repository handles this logic
    }

    override fun deleteAlbumList(): Completable {
        appDatabase.albumDao().deleteAll()
        return Completable.complete()
    }
}
