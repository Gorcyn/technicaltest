package com.gorcyn.technicaltest.data.source.remote

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.util.SingletonHolderSingleArg
import com.gorcyn.technicaltest.util.flatMapIterable

open class AlbumRemoteDataSource(
        private val service: AlbumService
): AlbumDataSource {

    companion object : SingletonHolderSingleArg<AlbumRemoteDataSource, AlbumService>(::AlbumRemoteDataSource)

    override fun fetchAlbumList(): Single<List<Album>> {
        return service.fetchAlbumList()
    }

    override fun fetchAlbum(albumId: Long): Maybe<Album> {
        return service.fetchAlbumList()
                .flatMapIterable()
                .filter { it.id == albumId }
                .firstElement()
    }

    override fun saveAlbum(album: Album): Completable {
        // Read only, for now
        return Completable.complete()
    }

    override fun refreshAlbumList() {
        // Repository handles this logic
    }

    override fun deleteAlbumList(): Completable {
        // Read only, for now
        return Completable.complete()
    }
}
