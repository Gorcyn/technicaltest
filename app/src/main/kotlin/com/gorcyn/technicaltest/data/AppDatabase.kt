package com.gorcyn.technicaltest.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

import com.gorcyn.technicaltest.data.dao.AlbumDao
import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.util.SingletonHolderSingleArg

@Database(entities = [(Album::class)], version = 1)
abstract class AppDatabase: RoomDatabase() {

    companion object : SingletonHolderSingleArg<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext,
                AppDatabase::class.java, "app.db")
                .build()
    })

    abstract fun albumDao(): AlbumDao
}
