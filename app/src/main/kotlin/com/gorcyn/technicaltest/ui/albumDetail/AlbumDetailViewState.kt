package com.gorcyn.technicaltest.ui.albumDetail

import com.gorcyn.technicaltest.data.entity.Album

data class AlbumDetailViewState(
        val isLoading: Boolean = false,
        val album: Album? = null,
        val error: Throwable? = null
)
