package com.gorcyn.technicaltest.ui

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

import com.gorcyn.technicaltest.R
import com.gorcyn.technicaltest.extensions.replaceFragment
import com.gorcyn.technicaltest.ui.albumList.AlbumListFragment

class MainActivity : AppCompatActivity() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.findFragmentById(R.id.container) ?: AlbumListFragment.newInstance().also {
            replaceFragment(it, R.id.container)
        }
    }
}
