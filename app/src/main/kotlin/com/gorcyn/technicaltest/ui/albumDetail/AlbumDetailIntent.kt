package com.gorcyn.technicaltest.ui.albumDetail

sealed class AlbumDetailIntent {
    data class InitialIntent(val albumId: Long) : AlbumDetailIntent()
    data class RefreshIntent(val albumId: Long, val forceUpdate: Boolean) : AlbumDetailIntent()
}
