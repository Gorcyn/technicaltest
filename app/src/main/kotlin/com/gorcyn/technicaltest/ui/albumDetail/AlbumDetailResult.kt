package com.gorcyn.technicaltest.ui.albumDetail

import com.gorcyn.technicaltest.data.entity.Album

sealed class AlbumDetailResult {

    sealed class LoadAlbumResult : AlbumDetailResult() {
        data class Success(val album: Album) : LoadAlbumResult()
        data class Failure(val error: Throwable) : LoadAlbumResult()
        object InFlight : LoadAlbumResult()
    }
}
