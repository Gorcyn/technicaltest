package com.gorcyn.technicaltest.ui.albumDetail

import io.reactivex.Observable
import io.reactivex.ObservableTransformer

import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailAction.LoadAlbumAction
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailResult.LoadAlbumResult
import com.gorcyn.technicaltest.util.schedulers.BaseSchedulerProvider

class AlbumDetailActionProcessorHolder(
        private val dataSource: AlbumDataSource,
        private val schedulerProvider: BaseSchedulerProvider
) {

    private val loadAlbumProcessor =
            ObservableTransformer<LoadAlbumAction, LoadAlbumResult> { actions ->
                actions.flatMap { action ->
                    dataSource.fetchAlbum(action.albumId)
                            .flatMapObservable { Observable.just(it) }
                            // Transform the Single to an Observable to allow emission of multiple
                            // events down the stream (e.g. the InFlight event)
                            // Wrap returned data into an immutable object
                            .map { LoadAlbumResult.Success(it) }
                            .cast(LoadAlbumResult::class.java)
                            // Wrap any error into an immutable object and pass it down the stream
                            // without crashing.
                            // Because errors are data and hence, should just be part of the stream.
                            .onErrorReturn(LoadAlbumResult::Failure)
                            .subscribeOn(schedulerProvider.io())
                            .observeOn(schedulerProvider.ui())
                            // Emit an InFlight event to notify the subscribers (e.g. the UI) we are
                            // doing work and waiting on a response.
                            // We emit it after observing on the UI thread to allow the event to be emitted
                            // on the current frame and avoid jank.
                            .startWith(LoadAlbumResult.InFlight)
                }
            }

    /**
     * Splits the [Observable] to match each type of [AlbumDetailAction] to
     * its corresponding business logic processor. Each processor takes a defined [AlbumDetailAction],
     * returns a defined [AlbumDetailResult]
     * The global actionProcessor then merges all [Observable] back to
     * one unique [Observable].
     *
     *
     * The splitting is done using [Observable.publish] which allows almost anything
     * on the passed [Observable] as long as one and only one [Observable] is returned.
     *
     *
     * An security layer is also added for unhandled [AlbumDetailAction] to allow early crash
     * at runtime to easy the maintenance.
     */
    internal var actionProcessor =
            ObservableTransformer<AlbumDetailAction, AlbumDetailResult> { actions ->
                actions.publish { shared ->
                    Observable.merge(
                            // Match LoadAlbumAction to loadAlbumProcessor
                            shared.ofType(AlbumDetailAction.LoadAlbumAction::class.java)
                                    .compose(loadAlbumProcessor),

                            // Error for not implemented actions
                            shared.filter { v ->
                                v !is AlbumDetailAction.LoadAlbumAction
                            }.flatMap { w ->
                                Observable.error<AlbumDetailResult>(
                                        IllegalArgumentException("Unknown Action type: $w"))
                            }
                    )
                }
            }
}
