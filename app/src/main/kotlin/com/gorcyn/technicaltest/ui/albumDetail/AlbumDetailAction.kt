package com.gorcyn.technicaltest.ui.albumDetail

sealed class AlbumDetailAction {
    data class LoadAlbumAction(val albumId: Long, val forceUpdate: Boolean): AlbumDetailAction()
}
