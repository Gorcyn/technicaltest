package com.gorcyn.technicaltest.ui.albumList

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout

import com.gorcyn.technicaltest.R
import com.gorcyn.technicaltest.extensions.addFragment
import com.gorcyn.technicaltest.extensions.snack
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailFragment
import com.gorcyn.technicaltest.util.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_album_list.albumListProgressBar
import kotlinx.android.synthetic.main.fragment_album_list.albumListRecyclerView
import kotlinx.android.synthetic.main.fragment_album_list.albumListToolbar

class AlbumListFragment: Fragment() {

    companion object {
        fun newInstance() = AlbumListFragment()
    }

    private var adapter = AlbumListAdapter()

    private val refreshIntentPublisher = PublishSubject.create<AlbumListIntent.RefreshIntent>()
    private val disposables = CompositeDisposable()

    private val viewModel: AlbumListViewModel by lazy {
        ViewModelProviders
                .of(this, ViewModelFactory.getInstance(context!!))
                .get(AlbumListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_album_list, container, false)
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(albumListToolbar)
        albumListRecyclerView.adapter = adapter
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
        bind()
    }

    /**
     * Connect the view with the [AlbumListViewModel]
     * We subscribe to the [AlbumListViewModel] before passing it the view's [AlbumListIntent]s.
     * If we were to pass [AlbumListIntent]s to the [AlbumListViewModel] before listening to it,
     * emitted [AlbumListViewState]s could be lost
     */
    private fun bind() {
        // Subscribe to the ViewModel and call render for every emitted state
        disposables.add(viewModel.states().subscribe({ render(it) }, {}))
        // Pass the UI's intents to the ViewModel
        viewModel.processIntents(intents())

        disposables.add(
            adapter.itemClickObservable.subscribe({ album ->
                showAlbumDetails(album.id)
            }, {})
        )
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        refreshIntentPublisher.onNext(AlbumListIntent.RefreshIntent(false))
    }

    @CallSuper
    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun intents(): Observable<AlbumListIntent> = Observable.merge(initialIntent(), refreshIntent())

    private fun render(state: AlbumListViewState) {
        albumListProgressBar.isRefreshing = state.isLoading
        if (state.error != null) {
            snack(state.error.localizedMessage)
        }

        if (state.isLoading) {
            val adapter = AlbumListPreviewAdapter(7)
            albumListRecyclerView.setOnTouchListener { _, _ -> true }
            albumListRecyclerView.adapter = adapter
        } else {
            albumListRecyclerView.adapter = adapter
            albumListRecyclerView.setOnTouchListener(null)
            adapter.albumList = state.albumList
            adapter.notifyDataSetChanged()
        }
    }

    /**
     * The initial Intent the view emit to convey to the [AlbumListViewModel]
     * that it is ready to receive data.
     * This initial Intent is also used to pass any parameters the [AlbumListViewModel] might need
     * to render the initial [AlbumListViewState] (e.g. the album id to load).
     */
    private fun initialIntent(): Observable<AlbumListIntent.InitialIntent> = Observable.just(AlbumListIntent.InitialIntent)

    private fun refreshIntent(): Observable<AlbumListIntent.RefreshIntent> = RxSwipeRefreshLayout.refreshes(albumListProgressBar)
                .map { AlbumListIntent.RefreshIntent(true) }
                .mergeWith(refreshIntentPublisher)

    private fun showAlbumDetails(albumId: Long) {
        val fragment = AlbumDetailFragment.newInstance(albumId)
        addFragment(fragment, R.id.container, fragment.javaClass.simpleName)
    }
}
