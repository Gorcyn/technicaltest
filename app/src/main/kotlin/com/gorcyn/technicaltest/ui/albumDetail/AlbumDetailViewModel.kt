package com.gorcyn.technicaltest.ui.albumDetail

import android.arch.lifecycle.ViewModel

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailResult.LoadAlbumResult
import com.gorcyn.technicaltest.util.notOfType

data class AlbumDetailViewModel(
        private val actionProcessorHolder: AlbumDetailActionProcessorHolder,
        val isLoading: Boolean = false,
        val albumList: List<Album> = emptyList(),
        val error: Throwable? = null
): ViewModel() {

    /**
     * Proxy subject used to keep the stream alive even after the UI gets recycled.
     * This is basically used to keep ongoing events and the last cached State alive
     * while the UI disconnects and reconnects on config changes.
     */
    private val intentsSubject: PublishSubject<AlbumDetailIntent> = PublishSubject.create()
    private val statesObservable: Observable<AlbumDetailViewState> = compose()

    /**
     * take only the first ever InitialIntent and all intents of other types
     * to avoid reloading data on config changes
     */
    private val intentFilter: ObservableTransformer<AlbumDetailIntent, AlbumDetailIntent>
        get() = ObservableTransformer { intents ->
            intents.publish<AlbumDetailIntent> { shared ->
                Observable.merge(
                        shared.ofType(AlbumDetailIntent.InitialIntent::class.java).take(1),
                        shared.notOfType(AlbumDetailIntent.InitialIntent::class.java)
                )
            }
        }

    fun processIntents(intents: Observable<AlbumDetailIntent>) = intents.subscribe(intentsSubject)

    fun states(): Observable<AlbumDetailViewState> = statesObservable

    private fun compose(): Observable<AlbumDetailViewState> = intentsSubject
            .compose(intentFilter)
            .map(this::actionFromIntent)
            .compose(actionProcessorHolder.actionProcessor)
            // Cache each state and pass it to the reducer to create a new state from
            // the previous cached one and the latest Result emitted from the action processor.
            // The Scan operator is used here for the caching.
            .scan(AlbumDetailViewState(), reducer)
            // When a reducer just emits previousState, there's no reason to call render. In fact,
            // redrawing the UI in cases like this can cause jank (e.g. messing up snackbar animations
            // by showing the same snackbar twice in rapid succession).
            .distinctUntilChanged()
            // Emit the last one event of the stream on subscription
            // Useful when a View rebinds to the ViewModel after rotation.
            .replay(1)
            // Create the stream on creation without waiting for anyone to subscribe
            // This allows the stream to stay alive even when the UI disconnects and
            // match the stream's lifecycle to the ViewModel's one.
            .autoConnect(0)

    private fun actionFromIntent(intent: AlbumDetailIntent): AlbumDetailAction = when(intent) {
        is AlbumDetailIntent.InitialIntent -> AlbumDetailAction.LoadAlbumAction(intent.albumId, true)
        is AlbumDetailIntent.RefreshIntent -> AlbumDetailAction.LoadAlbumAction(intent.albumId, intent.forceUpdate)
    }

    companion object {
        /**
         * The Reducer is where [AlbumDetailViewState], that the view will use to
         * render itself, are created.
         * It takes the last cached [AlbumDetailViewState], the latest [AlbumDetailResult] and
         * creates a new [AlbumDetailViewState] by only updating the related fields.
         * This is basically like a big switch statement of all possible types for the [AlbumDetailResult]
         */
        private val reducer = BiFunction { previousState: AlbumDetailViewState, result: AlbumDetailResult ->
            when (result) {
                is LoadAlbumResult -> when (result) {
                    is LoadAlbumResult.Success -> {
                        previousState.copy(isLoading = false, album = result.album)
                    }
                    is LoadAlbumResult.Failure -> previousState.copy(isLoading = false, error = result.error)
                    is LoadAlbumResult.InFlight -> previousState.copy(isLoading = true)
                }
            }
        }
    }
}
