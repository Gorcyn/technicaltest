package com.gorcyn.technicaltest.ui.albumList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

import com.gorcyn.technicaltest.R
import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.util.GlideApp

import kotlinx.android.synthetic.main.item_album.view.albumListAlbumTitle
import kotlinx.android.synthetic.main.item_album.view.albumListAlbumThumbnail

open class AlbumListAdapter: RecyclerView.Adapter<AlbumListAdapter.ViewHolder>() {

    private val itemClickSubject = PublishSubject.create<Album>()

    val itemClickObservable: Observable<Album>
        get() = itemClickSubject

    var albumList: List<Album> = emptyList()

    override fun getItemCount(): Int =  albumList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = albumList[position].title

        holder.itemView.setOnClickListener {
            itemClickSubject.onNext(albumList[position])
        }

        GlideApp.with(holder.itemView.context)
                .load(albumList[position].thumbnailUrl)
                .into(holder.thumbnail)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.albumListAlbumTitle
        val thumbnail: ImageView = itemView.albumListAlbumThumbnail
    }
}
