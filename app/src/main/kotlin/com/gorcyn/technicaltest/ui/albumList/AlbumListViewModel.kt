package com.gorcyn.technicaltest.ui.albumList

import android.arch.lifecycle.ViewModel

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.ui.albumList.AlbumListResult.LoadAlbumListResult
import com.gorcyn.technicaltest.util.notOfType

data class AlbumListViewModel(
        private val actionProcessorHolder: AlbumListActionProcessorHolder,
        val isLoading: Boolean = false,
        val albumList: List<Album> = emptyList(),
        val error: Throwable? = null
): ViewModel() {

    /**
     * Proxy subject used to keep the stream alive even after the UI gets recycled.
     * This is basically used to keep ongoing events and the last cached State alive
     * while the UI disconnects and reconnects on config changes.
     */
    private val intentsSubject: PublishSubject<AlbumListIntent> = PublishSubject.create()
    private val statesObservable: Observable<AlbumListViewState> = compose()

    /**
     * take only the first ever InitialIntent and all intents of other types
     * to avoid reloading data on config changes
     */
    private val intentFilter: ObservableTransformer<AlbumListIntent, AlbumListIntent>
        get() = ObservableTransformer { intents ->
            intents.publish<AlbumListIntent> { shared ->
                Observable.merge(
                        shared.ofType(AlbumListIntent.InitialIntent::class.java).take(1),
                        shared.notOfType(AlbumListIntent.InitialIntent::class.java)
                )
            }
        }

    fun processIntents(intents: Observable<AlbumListIntent>) = intents.subscribe(intentsSubject)

    fun states(): Observable<AlbumListViewState> = statesObservable

    private fun compose(): Observable<AlbumListViewState> = intentsSubject
            .compose(intentFilter)
            .map(this::actionFromIntent)
            .compose(actionProcessorHolder.actionProcessor)
            // Cache each state and pass it to the reducer to create a new state from
            // the previous cached one and the latest Result emitted from the action processor.
            // The Scan operator is used here for the caching.
            .scan(AlbumListViewState(), reducer)
            // When a reducer just emits previousState, there's no reason to call render. In fact,
            // redrawing the UI in cases like this can cause jank (e.g. messing up snackbar animations
            // by showing the same snackbar twice in rapid succession).
            .distinctUntilChanged()
            // Emit the last one event of the stream on subscription
            // Useful when a View rebinds to the ViewModel after rotation.
            .replay(1)
            // Create the stream on creation without waiting for anyone to subscribe
            // This allows the stream to stay alive even when the UI disconnects and
            // match the stream's lifecycle to the ViewModel's one.
            .autoConnect(0)

    private fun actionFromIntent(intent: AlbumListIntent): AlbumListAction = when(intent) {
        is AlbumListIntent.InitialIntent -> AlbumListAction.LoadAlbumListAction(true)
        is AlbumListIntent.RefreshIntent -> AlbumListAction.LoadAlbumListAction(intent.forceUpdate)
    }

    companion object {
        /**
         * The Reducer is where [AlbumListViewState], that the view will use to
         * render itself, are created.
         * It takes the last cached [AlbumListViewState], the latest [AlbumListResult] and
         * creates a new [AlbumListViewState] by only updating the related fields.
         * This is basically like a big switch statement of all possible types for the [AlbumListResult]
         */
        private val reducer = BiFunction { previousState: AlbumListViewState, result: AlbumListResult ->
            when (result) {
                is LoadAlbumListResult -> when (result) {
                    is LoadAlbumListResult.Success -> {
                        previousState.copy(isLoading = false, albumList = result.albumList)
                    }
                    is LoadAlbumListResult.Failure -> previousState.copy(isLoading = false, error = result.error)
                    is LoadAlbumListResult.InFlight -> previousState.copy(isLoading = true)
                }
            }
        }
    }
}
