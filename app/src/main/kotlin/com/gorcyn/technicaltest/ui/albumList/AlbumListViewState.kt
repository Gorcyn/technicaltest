package com.gorcyn.technicaltest.ui.albumList

import com.gorcyn.technicaltest.data.entity.Album

data class AlbumListViewState(
        val isLoading: Boolean = false,
        val albumList: List<Album> = emptyList(),
        val error: Throwable? = null
)
