package com.gorcyn.technicaltest.ui.albumList

sealed class AlbumListIntent {
    object InitialIntent : AlbumListIntent()
    data class RefreshIntent(val forceUpdate: Boolean) : AlbumListIntent()
}
