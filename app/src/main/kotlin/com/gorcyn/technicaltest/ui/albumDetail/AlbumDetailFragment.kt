package com.gorcyn.technicaltest.ui.albumDetail

import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout

import com.gorcyn.technicaltest.R
import com.gorcyn.technicaltest.extensions.snack
import com.gorcyn.technicaltest.util.GlideApp
import com.gorcyn.technicaltest.util.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_album_detail.albumDetailProgressBar
import kotlinx.android.synthetic.main.fragment_album_detail.albumDetailAlbumThumbnail
import kotlinx.android.synthetic.main.fragment_album_detail.albumDetailAlbumTitle

class AlbumDetailFragment: Fragment() {

    companion object {
        private const val ALBUM_ID = "id"

        fun newInstance(id: Long): AlbumDetailFragment {
            val bundle = Bundle()
            bundle.putLong(ALBUM_ID, id)
            val fragment = AlbumDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private val refreshIntentPublisher = PublishSubject.create<AlbumDetailIntent.RefreshIntent>()
    private val disposables = CompositeDisposable()

    private val viewModel: AlbumDetailViewModel by lazy {
        ViewModelProviders
                .of(this, ViewModelFactory.getInstance(context!!))
                .get(AlbumDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_album_detail, container, false)
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
        bind()
    }

    /**
     * Connect the view with the [AlbumDetailViewModel]
     * We subscribe to the [AlbumDetailViewModel] before passing it the view's [AlbumDetailIntent]s.
     * If we were to pass [AlbumDetailIntent]s to the [AlbumDetailViewModel] before listening to it,
     * emitted [AlbumDetailViewState]s could be lost
     */
    private fun bind() {
        // Subscribe to the ViewModel and call render for every emitted state
        disposables.add(viewModel.states().subscribe({ render(it) }, {}))
        // Pass the UI's intents to the ViewModel
        viewModel.processIntents(intents())
    }

    @CallSuper
    override fun onResume() {
        super.onResume()

        val albumId = arguments!!.getLong(ALBUM_ID)
        refreshIntentPublisher.onNext(AlbumDetailIntent.RefreshIntent(albumId, false))
    }

    @CallSuper
    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun intents(): Observable<AlbumDetailIntent> = Observable.merge(initialIntent(), refreshIntent())

    private fun render(state: AlbumDetailViewState) {
        albumDetailProgressBar.isRefreshing = state.isLoading
        if (state.error != null) {
            snack(state.error.localizedMessage)
        }

        // UI while loading
        albumDetailAlbumTitle.text = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            albumDetailAlbumTitle.setBackgroundColor(resources.getColor(R.color.lightGrey, context?.theme))
        } else {
            @Suppress("DEPRECATION")
            albumDetailAlbumTitle.setBackgroundColor(resources.getColor(R.color.lightGrey))
        }
        albumDetailAlbumThumbnail.setImageDrawable(null)
        if (!state.isLoading) {

            state.album?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    albumDetailAlbumTitle.background = null
                } else {
                    @Suppress("DEPRECATION")
                    albumDetailAlbumTitle.setBackgroundDrawable(null)
                }
                albumDetailAlbumTitle.text = it.title
                context?.let { context ->
                    GlideApp.with(context)
                            .load(it.url)
                            .into(albumDetailAlbumThumbnail)
                }
            }
        }
    }

    /**
     * The initial Intent the view emit to convey to the [AlbumDetailViewModel]
     * that it is ready to receive data.
     * This initial Intent is also used to pass any parameters the [AlbumDetailViewModel] might need
     * to render the initial [AlbumDetailViewState] (e.g. the album id to load).
     */
    private fun initialIntent(): Observable<AlbumDetailIntent.InitialIntent> = Observable.just(AlbumDetailIntent.InitialIntent(arguments!!.getLong(ALBUM_ID)))

    private fun refreshIntent(): Observable<AlbumDetailIntent.RefreshIntent> = RxSwipeRefreshLayout.refreshes(albumDetailProgressBar)
            .map { AlbumDetailIntent.RefreshIntent(arguments!!.getLong(ALBUM_ID), true) }
            .mergeWith(refreshIntentPublisher)
}
