package com.gorcyn.technicaltest.ui.albumList

import com.gorcyn.technicaltest.data.entity.Album

sealed class AlbumListResult {

    sealed class LoadAlbumListResult : AlbumListResult() {
        data class Success(val albumList: List<Album>) : LoadAlbumListResult()
        data class Failure(val error: Throwable) : LoadAlbumListResult()
        object InFlight : LoadAlbumListResult()
    }
}
