package com.gorcyn.technicaltest.ui.albumList

import android.view.LayoutInflater
import android.view.ViewGroup

import com.gorcyn.technicaltest.R

class AlbumListPreviewAdapter(
        private val size: Int
): AlbumListAdapter() {

    override fun getItemCount(): Int = size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_album_preview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = Unit
}
