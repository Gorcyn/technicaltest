package com.gorcyn.technicaltest.ui.albumList

import io.reactivex.Observable
import io.reactivex.ObservableTransformer

import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.ui.albumList.AlbumListAction.LoadAlbumListAction
import com.gorcyn.technicaltest.ui.albumList.AlbumListResult.LoadAlbumListResult
import com.gorcyn.technicaltest.util.schedulers.BaseSchedulerProvider

class AlbumListActionProcessorHolder(
        private val dataSource: AlbumDataSource,
        private val schedulerProvider: BaseSchedulerProvider
) {

    private val loadAlbumListProcessor =
            ObservableTransformer<LoadAlbumListAction, LoadAlbumListResult> { actions ->
                actions.flatMap { action ->
                    dataSource.fetchAlbumList(action.forceUpdate)
                            // Transform the Single to an Observable to allow emission of multiple
                            // events down the stream (e.g. the InFlight event)
                            .toObservable()
                            // Wrap returned data into an immutable object
                            .map { LoadAlbumListResult.Success(it) }
                            .cast(LoadAlbumListResult::class.java)
                            // Wrap any error into an immutable object and pass it down the stream
                            // without crashing.
                            // Because errors are data and hence, should just be part of the stream.
                            .onErrorReturn(LoadAlbumListResult::Failure)
                            .subscribeOn(schedulerProvider.io())
                            .observeOn(schedulerProvider.ui())
                            // Emit an InFlight event to notify the subscribers (e.g. the UI) we are
                            // doing work and waiting on a response.
                            // We emit it after observing on the UI thread to allow the event to be emitted
                            // on the current frame and avoid jank.
                            .startWith(LoadAlbumListResult.InFlight)
                }
            }

    /**
     * Splits the [Observable] to match each type of [AlbumListAction] to
     * its corresponding business logic processor. Each processor takes a defined [AlbumListAction],
     * returns a defined [AlbumListResult]
     * The global actionProcessor then merges all [Observable] back to
     * one unique [Observable].
     *
     *
     * The splitting is done using [Observable.publish] which allows almost anything
     * on the passed [Observable] as long as one and only one [Observable] is returned.
     *
     *
     * An security layer is also added for unhandled [AlbumListAction] to allow early crash
     * at runtime to easy the maintenance.
     */
    internal var actionProcessor =
            ObservableTransformer<AlbumListAction, AlbumListResult> { actions ->
                actions.publish { shared ->
                    Observable.merge(
                            // Match LoadAlbumListAction to loadAlbumListProcessor
                            shared.ofType(AlbumListAction.LoadAlbumListAction::class.java)
                                    .compose(loadAlbumListProcessor),

                            // Error for not implemented actions
                            shared.filter { v ->
                                v !is AlbumListAction.LoadAlbumListAction
                            }.flatMap { w ->
                                Observable.error<AlbumListResult>(
                                        IllegalArgumentException("Unknown Action type: $w"))
                            }
                    )
                }
            }
}
