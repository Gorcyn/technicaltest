package com.gorcyn.technicaltest.ui.albumList

sealed class AlbumListAction {
    data class LoadAlbumListAction(val forceUpdate: Boolean): AlbumListAction()
}
