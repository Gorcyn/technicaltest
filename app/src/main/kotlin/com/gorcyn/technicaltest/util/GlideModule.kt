package com.gorcyn.technicaltest.util

import com.bumptech.glide.annotation.GlideModule as BaseGlideModule
import com.bumptech.glide.module.AppGlideModule

@BaseGlideModule
class GlideModule: AppGlideModule()
