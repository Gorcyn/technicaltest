package com.gorcyn.technicaltest.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context

import com.gorcyn.technicaltest.Injection
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailActionProcessorHolder
import com.gorcyn.technicaltest.ui.albumDetail.AlbumDetailViewModel
import com.gorcyn.technicaltest.ui.albumList.AlbumListActionProcessorHolder
import com.gorcyn.technicaltest.ui.albumList.AlbumListViewModel

class ViewModelFactory private constructor(
        private val applicationContext: Context
): ViewModelProvider.Factory {

    companion object : SingletonHolderSingleArg<ViewModelFactory, Context>(::ViewModelFactory)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == AlbumListViewModel::class.java) {
            return AlbumListViewModel(
                    AlbumListActionProcessorHolder(
                            Injection.provideAlbumDataSource(applicationContext),
                            Injection.provideSchedulerProvider()
                    )
            ) as T
        }
        if (modelClass == AlbumDetailViewModel::class.java) {
            return AlbumDetailViewModel(
                    AlbumDetailActionProcessorHolder(
                            Injection.provideAlbumDataSource(applicationContext),
                            Injection.provideSchedulerProvider()
                    )
            ) as T
        }
        throw IllegalArgumentException("Unknown model class $modelClass")
    }
}
