package com.gorcyn.technicaltest.robot

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.extensions.canSee

fun albumDetail(func: AlbumDetailRobot.() -> Unit) {
    AlbumDetailRobot().apply { func() }
}

class AlbumDetailRobot {

    fun iSeeAnAlbum(album: Album) {
        album.title.canSee()
    }
}
