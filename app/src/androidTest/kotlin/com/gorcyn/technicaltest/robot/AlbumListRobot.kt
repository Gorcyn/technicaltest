package com.gorcyn.technicaltest.robot

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.extensions.canSee
import com.gorcyn.technicaltest.extensions.click

fun albumList(func: AlbumListRobot.() -> Unit) {
    AlbumListRobot().apply { func() }
}

class AlbumListRobot {

    fun iSeeAnAlbum(album: Album) {
        album.title.canSee()
    }

    fun iSelectAnAlbum(album: Album) {
        album.title.click()
    }
}
