package com.gorcyn.technicaltest.data

import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsCollectionContaining.hasItems

import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4

import io.reactivex.observers.TestObserver

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.data.source.local.AlbumLocalDataSource

@RunWith(AndroidJUnit4::class)
@LargeTest
class AlbumLocalDataSourceTest {

    private lateinit var localDataSource: AlbumLocalDataSource

    @Before
    fun setup() {
        AlbumLocalDataSource.clearInstance()

        localDataSource = AlbumLocalDataSource
                .getInstance(AppDatabase.getInstance(InstrumentationRegistry.getTargetContext()))
    }

    @After
    fun cleanUp() {
        localDataSource.deleteAlbumList()
    }

    @Test
    fun testPreConditions() {
        assertNotNull(localDataSource)
    }

    @Test
    fun saveAlbum_retrievesAlbum() {
        // Given a new album
        val newAlbum = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)

        // When saved into the persistent repository
        localDataSource.saveAlbum(newAlbum)

        // Then the album can be retrieved from the persistent repository
        val testObserver = TestObserver<Album>()
        localDataSource.fetchAlbum(newAlbum.id).subscribe(testObserver)
        testObserver.assertValue(newAlbum)
    }

    @Test
    fun deleteAlbumList_emptyListOfRetrievedAlbum() {
        // Given a new album in the persistent repository and a mocked callback
        val newAlbum = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)
        localDataSource.saveAlbum(newAlbum)

        // When all albums are deleted
        localDataSource.deleteAlbumList()

        // Then the retrieved albums is an empty list
        val testObserver = TestObserver<List<Album>>()
        localDataSource.fetchAlbumList().subscribe(testObserver)
        val result = testObserver.values()[0]
        assertThat(result.isEmpty(), `is`(true))
    }

    @Test
    fun getAlbumList_retrieveSavedAlbumList() {
        // Given 2 new albums in the persistent repository
        val newAlbum1 = Album(1, "TITLE", "URL", "THUMBNAIL_URL", 1)
        localDataSource.saveAlbum(newAlbum1)
        val newAlbum2 = Album(2, "TITLE", "URL", "THUMBNAIL_URL", 1)
        localDataSource.saveAlbum(newAlbum2)

        // Then the albums can be retrieved from the persistent repository
        val testObserver = TestObserver<List<Album>>()
        localDataSource.fetchAlbumList().subscribe(testObserver)
        val result = testObserver.values()[0]
        assertThat(result, hasItems(newAlbum1, newAlbum2))
    }

    @Test
    fun getAlbum_whenAlbumNotSaved() {
        //Given that no album has been saved
        //When querying for an album, null is returned.
        val testObserver = TestObserver<Album>()
        localDataSource.fetchAlbum(1).subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoValues()
    }
}
