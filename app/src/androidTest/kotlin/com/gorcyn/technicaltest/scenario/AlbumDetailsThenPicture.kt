package com.gorcyn.technicaltest.scenario

import org.junit.Test

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.robot.albumDetail
import com.gorcyn.technicaltest.robot.albumList
import com.gorcyn.technicaltest.tests.UITest

class AlbumDetailsThenPicture: UITest() {

    @Test
    fun selectAnAlbumToSeeItsDetails() {

        val album = Album(1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952", 1)
        albumList {
            iSeeAnAlbum(album)
            iSelectAnAlbum(album)
        }
        albumDetail {
            iSeeAnAlbum(album)
        }
    }
}
