@file:Suppress("unused")

package com.gorcyn.technicaltest.extensions

import org.hamcrest.Matchers.not

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingRootException
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.click as vClick
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.hasErrorText
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.isEnabled
import android.support.test.espresso.matcher.ViewMatchers.withText

private fun String.get(): ViewInteraction {
    return onView(withText(this))
}

fun String.canSee(): String {
    this.get().check(matches(isDisplayed()))
    return this
}

fun String.cannotSee(): String {
    try {
        this.get().check(matches(not(isDisplayed())))
    } catch (e: NoMatchingRootException) {
        this.get().check(doesNotExist())
    }
    return this
}

fun String.click(): String {
    this.get().perform(vClick())
    return this
}

fun String.isDisabled(): String {
    this.get().check(matches(not(isEnabled())))
    return this
}

fun String.type(text: String, releaseKeyboard: Boolean = false): String {
    this.get().perform(typeText(text))
    if (releaseKeyboard) {
        this.get().perform(closeSoftKeyboard())
    }
    return this
}

fun String.hasError(text: String): String {
    this.get().check(matches(hasErrorText(text)))
    return this
}