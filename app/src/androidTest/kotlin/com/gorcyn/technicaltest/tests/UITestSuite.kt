package com.gorcyn.technicaltest.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.technicaltest.data.AlbumLocalDataSourceTest
import com.gorcyn.technicaltest.scenario.AlbumDetailsThenPicture

@RunWith(Suite::class)

@Suite.SuiteClasses(
        AlbumDetailsThenPicture::class,
        AlbumLocalDataSourceTest::class
)
class UITestSuite
