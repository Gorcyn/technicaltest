package com.gorcyn.technicaltest.tests

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith

import android.content.Intent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4

import com.gorcyn.technicaltest.ui.MainActivity

@RunWith(AndroidJUnit4::class)
@LargeTest
abstract class UITest {

    @Suppress("unused")
    @get:Rule
    val intentsTestRule = IntentsTestRule(MainActivity::class.java, true, false)

    @Before
    fun beforeEach() {
        intentsTestRule.launchActivity(Intent())
    }

    @After
    fun afterEach() {
        intentsTestRule.activity.finish()
    }
}
