package com.gorcyn.technicaltest

import android.content.Context

import com.gorcyn.technicaltest.data.AppDatabase
import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.data.source.AlbumRepository
import com.gorcyn.technicaltest.data.source.local.AlbumLocalDataSource
import com.gorcyn.technicaltest.data.source.remote.AlbumRemoteDataSource
import com.gorcyn.technicaltest.data.source.remote.AlbumService
import com.gorcyn.technicaltest.util.schedulers.BaseSchedulerProvider
import com.gorcyn.technicaltest.util.schedulers.SchedulerProvider

class Injection {
    companion object {

        fun provideAlbumDataSource(context: Context): AlbumDataSource {
            return AlbumRepository.getInstance(
                    AlbumLocalDataSource.getInstance(AppDatabase.getInstance(context)),
                    AlbumRemoteDataSource.getInstance(AlbumService.instance)
            )
        }

        fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider
    }
}
