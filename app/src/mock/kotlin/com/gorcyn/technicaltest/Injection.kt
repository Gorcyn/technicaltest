package com.gorcyn.technicaltest

import android.content.Context

import com.gorcyn.technicaltest.data.source.AlbumDataSource
import com.gorcyn.technicaltest.data.source.FakeAlbumDataSource
import com.gorcyn.technicaltest.util.schedulers.BaseSchedulerProvider
import com.gorcyn.technicaltest.util.schedulers.SchedulerProvider

class Injection {

    companion object {
        fun provideAlbumDataSource(context: Context): AlbumDataSource {
            return FakeAlbumDataSource
        }

        fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider
    }
}
