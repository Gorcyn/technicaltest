package com.gorcyn.technicaltest.data.source

import java.util.concurrent.TimeUnit

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.Completable
import io.reactivex.Single

import com.gorcyn.technicaltest.data.entity.Album
import com.gorcyn.technicaltest.util.flatMapIterable

object FakeAlbumDataSource: AlbumDataSource {

    private val ALBUM_LIST = listOf(
            Album(1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952", 1),
            Album(2, "reprehenderit est deserunt velit ipsam", "https://via.placeholder.com/600/771796",  "https://via.placeholder.com/150/771796", 1),
            Album(3, "officia porro iure quia iusto qui ipsa ut modi", "https://via.placeholder.com/600/24f355", "https://via.placeholder.com/150/24f355", 1)
    )

    override fun fetchAlbumList(): Single<List<Album>> {
        return Single.timer(2000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { ALBUM_LIST }
    }

    override fun fetchAlbum(albumId: Long): Single<Album> {
        return Single.timer(2000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { ALBUM_LIST }
                .flatMapIterable()
                .filter { it.id == albumId }
                .firstOrError()
    }

    override fun saveAlbum(album: Album): Completable {
        // Not necessary
        return Completable.complete()
    }

    override fun refreshAlbumList() {
        // Repository handles this logic
    }

    override fun deleteAlbumList(): Completable {
        // Not necessary
        return Completable.complete()
    }
}
